*This article is copyright © Vertbyqb 2021 and is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/)*

Most people reading this have probably hear of Tor by now. It's a network that relays traffic between servers around the world to anonymize its users. This is what is known as a darknet, and Tor is just one of the different darknets that exist (Tor has become so well known that some use "darknet" to refer to Tor). One of the other darknets available is [I2P](https://en.wikipedia.org/wiki/I2P), or the Invisible Internet Project, which is what I'm talking about in this article. There are plenty of other darknets available, and I may try some of them in the future, but this article is dedicated to I2P.

Probably the greatest difference between I2P and Tor is that in I2P *everyone* is a node. On Tor you just send your traffic to certain known nodes, but on I2P you are also helping carry others' traffic through the I2P network. I2P is also less focused on allowing users to connect to the normal Internet. You can do this, through what is known as an outproxy, but there are very few of these and most browsing on I2P stays on I2P.

Another difference is I2P does not come with a browser bundle like Tor does, you have to set up a proxy manually. One reason for this is that I2P is less focused on HTTP. With Tor you can use different protocols, but it's not very common. On I2P different protocols are common, IRC and Bittorrent (which is recommended against using on Tor) are two common examples. Also, since everyone is a node there's a reason to leave I2P on when you're not browsing it.

# Getting on I2P

Although connecting to I2P is harder than connecting to Tor, it's still pretty easy to do. First you will need to chose a client. The [official client](https://geti2p.net/en/download) is written in Java, but there is also [i2pd](https://i2pd.website/), written in C++, which is what I will be using in this tutorial.

One more thing, you may want to set up i2pd on a separate device to run all the time if you use it somewhat often. I use a Raspberry Pi, but you could use any computer that can stay on for most of the day. Do keep in mind this is entirely optional though, you can run I2P on your main machine with no negative side-effects, you just won't be contributing as much to the I2P network

## Setting up i2pd

First you will need to install i2pd. If you use an Arch GNU+Linux based distribution (like [Parabola](https://www.parabola.nu/)) you can find it as `i2pd` in the main Arch Linux community repository. On Debian-based systems you may want to [add the i2pd repository](https://i2pd.readthedocs.io/en/latest/user-guide/install/#debian) to install it. It is available in the main Debian repositories, but it's very out-of-date (as is usual). For any other systems you can find instructions [here](https://i2pd.readthedocs.io/en/latest/user-guide/install/).

Now it's time for [configuration](https://i2pd.readthedocs.io/en/latest/user-guide/configuration/). The main configuration file should be at `/etc/i2pd/i2pd.conf`. This file accepts INI-like syntax, see link for exceptions, or you could just start editing the file, it's quite self-explanatory. First I will edit the Web Console settings, down in the `http` section. If you are running this on your main machine you won't need this.

```ini
[http]
address = <LOCAL IP ADDRESS>
port = 7070
auth = true
user = i2pd
pass = ins3curepAs5w0rd
```

From the default configuration I have changed the `address` value to the IP of the device on my local network. This is necessary to be able to access this from other machines on your local network. I also added `auth = true` and a username and password. This is so other devices on the local network can't access the web console without a password. If you are the only one on your local network you can delete this.

Next I changed the `httpproxy` section, which sets up the HTTP proxy. This will be needed for browsing normal HTTP I2P sites, so it's important to get right.

```ini
[httpproxy]
address = <SAME LOCAL IP ADDRESS>
port = 4444
outproxy = http://purokishi.i2p
```

Here I just changed the `address` value for the same reason I did in the `http` section. I also added the address of an outproxy. This is where I2P will route any addresses not on the I2P network to. So for example, one could navigate to `https://gnu.org` and it would take the traffic through I2P, to `purokishi.i2p`, and out on the normal Internet to `gnu.org`. 

And that's it! If you are running it on a separate machine and don't need any outproxies then the default configuration is fine. Now we can move on to starting i2pd and actually connecting to I2P!

## Running i2pd

To run i2pd you probably want to run it as a system daemon (hence the name - i2p**d**). This is easy to do. The systemd service script will automatically install with i2pd, and Artix's `galaxy` repository provides additional service scripts for Dinit, OpenRC, runit, s6, and Suite66. I will cover systemd and OpenRC, but for any other init systems it should be as simple as adding and starting the `i2pd` service.

To add and start i2pd with OpenRC, run the following commands as root:
```bash
rc-update add i2pd default
rc-service i2pd start
```

For systemd run these commands as root:
```bash
systemctl enable i2pd.service
systemctl start i2pd.service
```

Now you can connect to the machine on port 7070 and you should hopefully see a page that says "i2pd webconsole".

*Note: from here on out it is assumed that if you make a change to one of the configuration files you should restart the service to apply the change. OpenRC and systemd both accept the `restart` command in place of the `start` command to restart the service.*

## Configuring a Web Browser

Now that you are connected to I2P you will need something to browse it with. In the configuration we set up the HTTP proxy, so you can just proxy [your favorite web browser](https://www.gnu.org/software/gnuzilla/) to that and you can start browsing I2P.

Hold up though, before you do that you should create a separate browser profile. If you use your normal browser and didn't set up a outproxy you won't be able to connect to all your usual websites, and if you did set it up it will still be unbearably slow.

Luckily this is pretty easy to do in Firefox-based browsers (that is what I use, if you'd like to submit instructions on how to set up profiles and proxies on other browsers please contact me). Just go to `about:profiles`, click `Create a New Profile`, click `Next`, enter a profile name (I suggest `I2P`), and click `Finish`. This will create the new profile we will use for browsing I2P. Somewhat annoyingly it also sets new profiles to be the default profile, so (unless you want to browse I2P all the time) click the `Set as default profile` button on your previously default profile (it should say "This is the profile in use and it cannot be deleted").

To launch this new profile click the `Launch profile in new browser` button. Go to `about:preferences` and click the `Settings...` button in Network Settings at the bottom of the General tab. Here you should click the `Manual proxy configuration` button and enter the IP and port (`4444` by default) of the machine i2pd is running on. If it is running on your local machine, put in `localhost` (or `127.0.0.1`) as the IP. You should also check the `Also use this proxy for FTP and HTTPS` box and add the IP of the machine i2pd is running on to the `No proxy for` box, if it's running on a separate machine.

Now we finally have a web browser that can view HTTP I2P sites! You may also want to make a shortcut to launch your I2P browser easier. In Firefox you can add the `-P` flag with the name of the profile you created to launch that profile, so to launch [GNU Icecat](https://www.gnu.org/software/gnuzilla/) in the `I2P` profile you would run: `icecat -P I2P`. You can use that to create a script or desktop file that will run it for you, but that's beyond the scope of this tutorial.

*Note: On Firefox-based browsers you might want to set `keyword.enabled` to `false` in `about:config` to make it easier to manually go to `.i2p` domains, otherwise it will try to search them.*

## Useful I2P sites

- http://identiguy.i2p/ - a list of known I2P sites. You can use this to find new I2P sites or even submit your own I2P site so others can discover it.
- http://i2p-projekt.i2p - the I2P site version of the main I2P website.
- http://legwork.i2p/ - an I2P search engine
- http://0xff.i2p/ - a file sharing and URL shortening service based on 0x0.st with no restrictions other than the file size.

## Note about I2P sites

I2P sites (previously called eepsites) come in two versions, the human-readable version and the b32 version. All I2P sites will have a b32 version, but some sites will also get a human-readable version that's easy to memorize. As the I2P project says it [on the FAQ](https://geti2p.net/en/faq#subscriptions):

> Human-readable addresses such as http://website.i2p are references to a long, random string known as a destination. These references are registered and stored at address book services such as stats.i2p, which is run by zzz. You will often encounter a "b32" address. A "b32" is a hash (specifically, a SHA256 hash) of the destination. This hash is appended with ".b32.i2p" and serves as a convenient way to link to your hidden service, without requiring any registration on an address book service.

Also, all HTTP I2P sites run plain HTTP. The encryption automatically included in I2P is sufficient to hide communications, there's no need for the additional encryption of HTTPS.

# Beyond HTTP

So far I have covered how to get i2pd running and connect a browser to it's HTTP proxy. But as I mentioned before I2P is way more than just HTTP. This is more advanced than setting up HTTP and not everyone will need it, but it's here if you want it.

*I was going to have a section on setting up Bittorrent here but I could not figure it out and that was basically all that was holding me back from releasing this article. If I figure it out I might make another article on it.*

## IRC

To set up IRC you will need to open the `/etc/i2pd/tunnels.conf` file. This file has a set of tunnels that can either link an I2P site to a local IP and port, or link a local IP and port to an I2P site (more on that later). For now though you can just uncomment the `IRC-ILITA` and `IRC-IRC2P` blocks, changing the address if necessary:
```ini
[IRC-ILITA]
type = client
address = <LOCAL IP ADDRESS>
port = 6668
destination = irc.ilita.i2p
destinationport = 6667
keys = irc-keys.dat

[IRC-IRC2P]
type = client
address = <LOCAL IP ADDRESS>
port = 6669
destination = irc.postman.i2p
destinationport = 6667
keys = irc-keys.dat
```

This will set up two IRC networks on I2P, ILITA and IRC2P. IRC2P seems to be more commonly used though, and it has the #i2p support room, so I suggest you start there. Obviously if you have no need for one of these IRC servers you can just keep it commented out.

Now all you need to do is connect to the IP address and port you specified above and it will mostly work like any other IRC network. One note, make sure you are NOT using SSL, it will not work trying to use SSL (again, further encryption is not needed as it's all going through I2P).

# Making your own I2P site

Once you have I2P running it's actually very easy to set up an I2P site. Just like Tor there is no need for port forwarding to the outside Internet, as all traffic is going through I2P, and this actually makes it easier to run for some users than a conventional website.

First and foremost you must already have some server running on a defined IP and port. Again, I2P doesn't really care what type of server this is. Once you have that, it's as simple as adding a block to `/etc/i2pd/tunnels.conf`:
```ini
[anon-website]
type = http
host = 127.0.0.1
port = 80
keys = anon-website.dat
```

This will forward the server running on port 80 of the local machine to I2P. The `type` specifies it as `http`, more types can be found [here](https://i2pd.readthedocs.io/en/latest/user-guide/tunnels/#tunnel-types). The section name will help you identify what the server's b32 address is, so pick a fitting name for your service. Then just go to the webconsole, which should be at port 7070 of the IP address you are running i2pd on. Here you can click on the `I2P tunnels` tab, which will show you all the tunnels you set up in `tunnels.conf`, plus the proxies. Here in the "Server Tunnels" section there should be an entry with the name of the section you put in `tunnels.conf`, in my case `anon-website`, with a corresponding b32 link.

This next step is optional (and untested by me, I have no use for it yet), but you can get a human-readable, memorable address for your I2P site by registering it at <http://inr.i2p/postkey/>.
